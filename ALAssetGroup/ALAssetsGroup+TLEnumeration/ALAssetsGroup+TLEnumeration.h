#import <AssetsLibrary/AssetsLibrary.h>

@interface ALAssetsGroup (TLEnumeration)

// assume camera roll is k-ordered by date
- (void)enumerateAssetsReverseKOrdered:(ALAssetsGroupEnumerationResultsBlock)enumerationBlock;

@end
