#import "ALAssetsGroup+TLEnumeration.h"

MAKE_CATEGORIES_LOADABLE(ALAssetsGroup_TLEnumeration)

@implementation ALAssetsGroup (TLEnumeration)

- (void)enumerateAssetsReverseKOrdered:(ALAssetsGroupEnumerationResultsBlock)enumerationBlock
{
    const NSUInteger k = 100;
    __block NSUInteger bufferedIndex = 0;
    __block BOOL bufferedStop = NO; // enumerateAssetsWithOptions can set stop so don't pass it directly
    __block NSMutableArray *buffer = [NSMutableArray arrayWithCapacity:k];

    [self enumerateAssetsWithOptions:NSEnumerationReverse usingBlock:^(ALAsset *asset, NSUInteger index, BOOL *stop) {
        if (!asset || [buffer count] == k) {
            [buffer sortUsingComparator:^NSComparisonResult(ALAsset *obj1, ALAsset *obj2) {
                return [[obj2 valueForProperty:ALAssetPropertyDate] compare:[obj1 valueForProperty:ALAssetPropertyDate]];
            }];
        }
        
        if (asset) {
            if ([buffer count] == k) {
                enumerationBlock(buffer[0], bufferedIndex++, &bufferedStop);
                *stop = bufferedStop;
                [buffer removeObjectAtIndex:0];
            }
            
            [buffer addObject:asset];
        } else {
            for (ALAsset *nextAsset in buffer) {
                if (bufferedStop) break;
                enumerationBlock(nextAsset, bufferedIndex++, &bufferedStop);
            }
            enumerationBlock(nil, index, stop); // pass original ending params through
        }
    }];
}

@end
