//
//  NSArray+DeepMutableCopy.h
//  Ahrefs
//
//  Created by Dmitry Svishchov on 20/08/12
//  Copyright (c) 2012 Prophonix. All rights reserved
//


@interface NSArray(DeepMutableCopy)

//! Deep mutable copy
- (NSMutableArray*) deepMutableCopy;

@end