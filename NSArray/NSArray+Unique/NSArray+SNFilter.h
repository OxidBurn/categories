//
//  NSArray+SNFilter.h
//  ProgressSpinnerProject
//
//  Created by Sergey on 5/31/17.
//  Copyright © 2017 Sergey. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (SNFilter)

- (BOOL)hasDuplicateValueByKey:(NSString *)key;

@end
