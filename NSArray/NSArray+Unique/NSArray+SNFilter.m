//
//  NSArray+SNFilter.m
//  ProgressSpinnerProject
//
//  Created by Sergey on 5/31/17.
//  Copyright © 2017 Sergey. All rights reserved.
//

#import "NSArray+SNFilter.h"

@implementation NSArray (SNFilter)

- (BOOL)hasDuplicateValueByKey:(NSString *)key {
    NSArray* uniqueNames = [self valueForKeyPath:[NSString stringWithFormat:@"@distinctUnionOfObjects.%@",key]];
    return uniqueNames.count == self.count ? NO : YES;
}

@end
