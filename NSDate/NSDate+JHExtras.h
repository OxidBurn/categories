//
//  NSDate+JHExtras.h
//  MyDieline
//
//  Created by Josh Hudnall on 6/3/12.
//  Copyright (c) 2012 Method Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (JHExtras)

- (NSString *)jh_relativeDateSince:(NSDate *)origDate;

@end
