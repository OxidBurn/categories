//
//  NSDate+JHExtras.m
//  MyDieline
//
//  Created by Josh Hudnall on 6/3/12.
//  Copyright (c) 2012 Method Apps. All rights reserved.
//

#import "NSDate+JHExtras.h"

@implementation NSDate (JHExtras)

- (NSString *)jh_relativeDateSince:(NSDate *)origDate {
    NSDate *todayDate = [NSDate date];
    double ti = [origDate timeIntervalSinceDate:todayDate];
    ti = ti * -1;
    if(ti < 1) {
        return @"now";
    } else if (ti < 60) {
        return @"now";
    } else if (ti < 60 * 60) {
        int diff = round(ti / 60);
        return [NSString stringWithFormat:@"%dm", diff];
    } else if (ti < 60 * 60 * 24) {
        int diff = round(ti / 60 / 60);
        return[NSString stringWithFormat:@"%dh", diff];
    } else if (ti < 60 * 60 * 24 * 7) {
        int diff = round(ti / 60 / 60 / 24);
        return[NSString stringWithFormat:@"%dd", diff];
    } else {
        int diff = round(ti / 60 / 60 / 24 / 7);
        return[NSString stringWithFormat:@"%dw", diff];
    }   
}

@end
