//
//  SAActionBlocks.h
//  
//
//  Created by Toby Herbert on 23/11/2012.
//
//  © SynAppsDev - http://www.syn-apps.co.uk/
//  support@syn-apps.co.uk

@interface NSObject (SAActionBlocks)

- (void)performBlockAfterDelay:(NSTimeInterval)delay block:(void (^)())block;
- (void)performBlockOnUniqueBackgroundQueueAfterDelay:(NSTimeInterval)delay block:(void (^)())block;
- (void)performBlockOnOperationQueue:(NSOperationQueue *)operationQueue afterDelay:(NSTimeInterval)delay block:(void (^)())block;
- (void)performBlockOnGCDQueue:(dispatch_queue_t)gcdQueue afterDelay:(NSTimeInterval)delay block:(void (^)())block;
- (void)performBlockOnThread:(NSThread *)thread afterDelay:(NSTimeInterval)delay block:(void (^)())block;

@end
