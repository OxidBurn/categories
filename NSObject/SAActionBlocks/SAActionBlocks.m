//
//  SAActionBlocks.m
//
//
//  Created by Toby Herbert on 23/11/2012.
//
//  © SynAppsDev - http://www.syn-apps.co.uk/
//  support@syn-apps.co.uk

#import "SAActionBlocks.h"

@implementation NSObject (SAActionBlocks)

- (void)performBlockAfterDelay:(NSTimeInterval)delay block:(void (^)())block
{
    NSOperationQueue *mainQueue = [NSOperationQueue mainQueue];
    [self performBlockOnOperationQueue:mainQueue afterDelay:delay block:block];
}

- (void)performBlockOnUniqueBackgroundQueueAfterDelay:(NSTimeInterval)delay block:(void (^)())block
{
    NSOperationQueue *backgroundQueue = [[NSOperationQueue alloc] init];
    [self performBlockOnOperationQueue:backgroundQueue afterDelay:delay block:block];
#if !__has_feature(objc_arc)
    [backgroundQueue release];
#endif
}

- (void)performBlockOnOperationQueue:(NSOperationQueue *)operationQueue afterDelay:(NSTimeInterval)delay block:(void (^)())block
{
    NSOperationQueue *backgroundQueue = [[NSOperationQueue alloc] init];
    [backgroundQueue addOperationWithBlock:^{
        sleep(delay);
        [operationQueue addOperationWithBlock:block];
    }];
#if !__has_feature(objc_arc)
    [backgroundQueue release];
#endif
}

- (void)performBlockOnGCDQueue:(dispatch_queue_t)gcdQueue afterDelay:(NSTimeInterval)delay block:(void (^)())block
{
    dispatch_queue_t backgroundQueue = dispatch_queue_create(NULL, NULL);
    dispatch_async(backgroundQueue, ^{
        sleep(delay);
        dispatch_async(gcdQueue, block);
    });
}

- (void)performBlockOnThread:(NSThread *)thread afterDelay:(NSTimeInterval)delay block:(void (^)())block
{
    [self performBlockOnUniqueBackgroundQueueAfterDelay:delay block:^{
        [self performSelector:@selector(_callBlock:) onThread:thread withObject:[block copy] waitUntilDone:NO];
    }];
}

- (void)_callBlock:(void (^)())block
{
    block();
}

@end