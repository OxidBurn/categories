//
//  NSString+MyProperty.h
//  ыыы
//
//  Created by Chaban Nikolay on 12/25/13.
//  Copyright (c) 2013 On Sight. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (MyProperty)

@property(assign, nonatomic) NSInteger myProperty;

@end
