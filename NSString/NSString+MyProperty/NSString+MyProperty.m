//
//  NSString+MyProperty.m
//  ыыы
//
//  Created by Chaban Nikolay on 12/25/13.
//  Copyright (c) 2013 On Sight. All rights reserved.
//

#import "NSString+MyProperty.h"
#import <objc/runtime.h>

static NSString *propertyKeyName = @"kMyProperty";

@implementation NSString (MyProperty)

- (void)setMyProperty:(NSInteger)propertyValue {
    objc_setAssociatedObject(self, (__bridge const void *)(propertyKeyName), [NSNumber numberWithInteger:propertyValue], OBJC_ASSOCIATION_ASSIGN);
}

- (NSInteger)myProperty {
    id propertyValue = objc_getAssociatedObject(self, (__bridge const void *)(propertyKeyName));
    return ((NSNumber *)propertyValue).intValue;
}

@end
