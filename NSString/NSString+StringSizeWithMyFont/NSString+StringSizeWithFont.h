//
// Created by Charles Guerin on 2014-06-04.
// Copyright (c) 2014 OBDS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NSString (StringSizeWithFont)

- (CGSize) sizeWithMyFont: (UIFont*) fontToUse;

- (CGSize) mySizeWithFont: (UIFont*) font;

- (CGSize) mySizeWithFont: (UIFont*) font
        constrainedToSize: (CGSize)  size;

- (CGSize) mySizeWithFont: (UIFont*)         font
        constrainedToSize: (CGSize)          size
            lineBreakMode: (NSLineBreakMode) lineBreakMode;
@end