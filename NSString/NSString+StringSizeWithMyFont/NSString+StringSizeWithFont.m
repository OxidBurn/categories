//
// Created by Charles Guerin on 2014-06-04.
// Copyright (c) 2014 OBDS. All rights reserved.
//

#import "NSString+StringSizeWithFont.h"


@implementation NSString (StringSizeWithFont)

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

- (CGSize) sizeWithMyFont: (UIFont*) fontToUse
{
    if ([self respondsToSelector: @selector(sizeWithAttributes:)] && fontToUse)
    {
        NSDictionary* attribs = @{NSFontAttributeName : fontToUse};
        
        return ([self sizeWithAttributes: attribs]);
    }
    
    return ([self sizeWithFont: fontToUse]);
}

#pragma GCC diagnostic pop


- (CGSize) mySizeWithFont: (UIFont*) font
{
    CGSize size = CGSizeMake(MAXFLOAT, MAXFLOAT);
    
    return [self mySizeWithFont: font
              constrainedToSize: size];

}

- (CGSize) mySizeWithFont: (UIFont*) font
        constrainedToSize: (CGSize)  size
{

    NSDictionary* attributesDictionary = @{NSFontAttributeName : font};
    
    CGRect frame = [self boundingRectWithSize: size
                                      options: (NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading)
                                   attributes: attributesDictionary
                                      context: nil];
    
    return frame.size;
}

- (CGSize) mySizeWithFont: (UIFont*)         font
        constrainedToSize: (CGSize)          size
            lineBreakMode: (NSLineBreakMode) lineBreakMode
{
    NSDictionary* attributesDictionary = @{NSFontAttributeName : font};
    
    CGRect frame = [self boundingRectWithSize: size
                                      options: (NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading)
                                   attributes: attributesDictionary
                                      context: nil];
    return frame.size;
}
@end


