//
//  UIImage+Resizing.h
//  Ahrefs
//
//  Created by Dmitry Svishchov on 20/08/12
//  Copyright (c) 2012 Prophonix. All rights reserved
//


@interface UIImage (Resizing)

//! Resize image to the specified size
- (UIImage*) imageResizedToSize: (CGSize) size
                       inPoints: (BOOL)   usePoints
            preserveAspectRatio: (BOOL)   preserveAspectRatio;

//! Cropped image
- (UIImage*) imageCroppedToFrame: (CGRect) frame;

@end