//
//  UIImage+Resizing.m
//  Ahrefs
//
//  Created by Dmitry Svishchov on 20/08/12
//  Copyright (c) 2012 Prophonix. All rights reserved
//

#import "UIImage+Resizing.h"


@implementation UIImage(Resizing)


- (UIImage*) imageResizedToSize: (CGSize) size
										   inPoints: (BOOL)   usePoints
            preserveAspectRatio: (BOOL)   preserveAspectRatio
{
	// Check for Retina display and then double the size of image (we assume size is in points)
	if (usePoints && [[UIScreen mainScreen] respondsToSelector: @selector(scale)])
	{
		CGFloat scale = [[UIScreen mainScreen] scale];
		
		size.width  *= scale;
		size.height *= scale;
	}
  
  CGSize newSize = size;
  
  // Preserve aspect ratio if required
  if (preserveAspectRatio)
  {
    if (self.size.width > self.size.height)
      newSize.width  = size.height * (self.size.width  / self.size.height);
    else
      newSize.height = size.width  * (self.size.height / self.size.width);
  }

	// Create context on which image will be drawn
	UIGraphicsBeginImageContext(newSize);
	
	// Draw image on this context used provided size
	[self drawInRect: CGRectMake(0, 0, newSize.width, newSize.height)];
	
	// Convert context to an image
	UIImage* resizedImage = UIGraphicsGetImageFromCurrentImageContext();    

	// Remove context
	UIGraphicsEndImageContext();
  
  // Resize to preserve aspect ratio
  if (preserveAspectRatio)
    resizedImage = [resizedImage imageCroppedToFrame: CGRectMake(0, 0, size.width, size.height)];
	
	
	return resizedImage;
}


- (UIImage*) imageCroppedToFrame: (CGRect) frame
{
	CGImageRef image = CGImageCreateWithImageInRect(self.CGImage, frame);
	
	UIImage* croppedImage = [UIImage imageWithCGImage: image]; 
	
	CGImageRelease(image);
	
	return croppedImage;
}


@end