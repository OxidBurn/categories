//
//  UIImage+ThreadSafeLoad.h
//  Viber
//
//  Created by Павел Трофимук on 04.05.12.
//  Copyright (c) 2012 `. All rights reserved.
//

#import <UIKit/UIKit.h>

struct UIImageDisplayOptions {
    CGFloat     strokeWidth;
    CGColorRef  strokeColor;
    CGFloat     roundRadius;
};
typedef struct UIImageDisplayOptions UIImageDisplayOptions;

CG_INLINE const UIImageDisplayOptions UIImageDisplayOptionsMake(CGFloat aStrokeWidth, UIColor *aStrokeColor, CGFloat aRoundRadius) {
    return (UIImageDisplayOptions){aStrokeWidth, [aStrokeColor CGColor], aRoundRadius};
}
CG_EXTERN const UIImageDisplayOptions UIImageDisplayNoOptions;

@interface UIImage (ThreadSafeLoad)

+ (UIImage *)threadSafeImageWithContentsOfFile:(NSString *)aFilepath;

// final size will be increase for stroke width
+ (UIImage *)threadSafeImageWithContentsOfFile:(NSString *)aFilepath finalSize:(CGSize)aSize withOptions:(UIImageDisplayOptions)options;

// calls threadSafeImageWithContentsOfFile: finalSize: strokeWidth: strokeColor: roundRadius: withFixedOrientation:
// withFixedOrientation - specifies the need to check the orientation of the image (problems that occur when you open photos taken from iPod)
+ (UIImage *)threadSafeImageWithContentsOfFile:(NSString *)aFilepath withFixedOrientation:(BOOL)isNeedFixedOrientation;

// it uses integral values for non-retina display

+ (UIImage *)threadSafeImageWithContentsOfFile:(NSString *)aFilepath finalSize:(CGSize)aSize;

// final size will be increase for stroke width
+ (UIImage *)threadSafeImageWithContentsOfFile:(NSString *)aFilepath finalSize:(CGSize)aSize strokeWidth:(CGFloat)aStrokeWidth strokeColor:(UIColor *)aStrokeColor roundRadius:(CGFloat)aRoundRadius;

+ (UIImage *)threadSafeImageWithContentsOfFile:(NSString *)aFilepath finalSize:(CGSize)aSize strokeWidth:(CGFloat)aStrokeWidth strokeColor:(UIColor *)aStrokeColor roundRadius:(CGFloat)aRoundRadius withFixedOrientation:(BOOL)isNeedFixedOrientation;

@end
