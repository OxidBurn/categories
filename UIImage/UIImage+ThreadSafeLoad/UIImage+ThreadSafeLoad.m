//
//  UIImage+ThreadSafeLoad.m
//  Viber
//
//  Created by Павел Трофимук on 04.05.12.
//  Copyright (c) 2012 `. All rights reserved.
//

#import "UIImage+ThreadSafeLoad.h"

#import <ImageIO/ImageIO.h>
#import "PathDrawing.h"
#import "debuglog.h"
#import "Utils.h"

const UIImageDisplayOptions UIImageDisplayNoOptions = {NAN, nil, NAN};

@implementation UIImage (ThreadSafeLoad)

+ (UIImage *)threadSafeImageWithContentsOfFile:(NSString *)aFilepath withFixedOrientation:(BOOL)isNeedFixedOrientation {
    return [self threadSafeImageWithContentsOfFile:aFilepath finalSize:CGSizeZero strokeWidth:0.0 strokeColor:nil roundRadius:0.0 withFixedOrientation:isNeedFixedOrientation];
}

+ (UIImage *)threadSafeImageWithContentsOfFile:(NSString *)aFilepath finalSize:(CGSize)aSize withOptions:(UIImageDisplayOptions)options {
    
    return [self threadSafeImageWithContentsOfFile:aFilepath finalSize:aSize strokeWidth:options.strokeWidth strokeColor:[UIColor colorWithCGColor:options.strokeColor] roundRadius:options.roundRadius];
}

+ (UIImage *)threadSafeImageWithContentsOfFile:(NSString *)aFilepath {
    return [self threadSafeImageWithContentsOfFile:aFilepath finalSize:CGSizeZero];
}

+ (UIImage *)threadSafeImageWithContentsOfFile:(NSString *)aFilepath finalSize:(CGSize)aSize {
    return [self threadSafeImageWithContentsOfFile:aFilepath finalSize:aSize strokeWidth:0.0 strokeColor:nil roundRadius:0.0];
}

+ (UIImage *)threadSafeImageWithContentsOfFile:(NSString *)aFilepath finalSize:(CGSize)aSize strokeWidth:(CGFloat)aStrokeWidth strokeColor:(UIColor *)aStrokeColor roundRadius:(CGFloat)aRoundRadius {
    return [self threadSafeImageWithContentsOfFile:aFilepath finalSize:aSize strokeWidth:aStrokeWidth strokeColor:aStrokeColor roundRadius:aRoundRadius withFixedOrientation:NO];
}

+ (UIImage *)threadSafeImageWithContentsOfFile:(NSString *)aFilepath finalSize:(CGSize)aSize strokeWidth:(CGFloat)aStrokeWidth strokeColor:(UIColor *)aStrokeColor roundRadius:(CGFloat)aRoundRadius withFixedOrientation:(BOOL)isNeedFixedOrientation {
    if (!aFilepath) {
        return nil;
    }
    
    // get a data provider referencing the relevant file
    CGDataProviderRef dataProvider = CGDataProviderCreateWithFilename([aFilepath UTF8String]);
    if (!dataProvider) {
        DebugLog(@"Warning: Invalid data provider for %@", aFilepath);
        return nil;
    }
    // use the data provider to get a CGImage; release the data provider
    
    CGImageRef newImage = NULL;
    if ([[aFilepath pathExtension] isEqualToString:@"png"]) {
        newImage = CGImageCreateWithPNGDataProvider(dataProvider, NULL, YES,
                                                    kCGRenderingIntentDefault);
    }
    else {
        newImage = CGImageCreateWithJPEGDataProvider(dataProvider, NULL, YES,
                                                     kCGRenderingIntentDefault);
    }
    
    CGDataProviderRelease(dataProvider);
    if (!newImage) {
        DebugLog(@"Warning: Invalid image from data provider for %@", aFilepath);
        return nil;
    }
    
    NSInteger retinaMultiplier = isRetinaDisplay() ? 2 : 1;
    BOOL useIntegralFrames = (!isRetinaDisplay());
    
    if (CGSizeEqualToSize(aSize, CGSizeZero)) {
        aSize = CGSizeMake((CGFloat)CGImageGetWidth(newImage) / retinaMultiplier, (CGFloat)CGImageGetHeight(newImage) / retinaMultiplier);
    }
    
    CGFloat anStrokeWidth = aStrokeWidth * retinaMultiplier;
    CGFloat anRoundRadius = aRoundRadius * retinaMultiplier;
    
    CGRect imageFrame = CGRectZero;
    imageFrame.size = aSize;
    
    BOOL useStroke = (isnan(anStrokeWidth) && anStrokeWidth != 0 && aStrokeColor != nil);
    if (useStroke) {
        imageFrame = CGRectInset(imageFrame, anStrokeWidth, anStrokeWidth);
    }
    if (useIntegralFrames) {
        imageFrame = CGRectIntegral(imageFrame);
    }
    imageFrame.size = CGSizeMake(imageFrame.size.width * retinaMultiplier, imageFrame.size.height * retinaMultiplier);
    
    // make a bitmap context of a suitable size to draw to, forcing decode
    unsigned char *imageBuffer = (unsigned char *)malloc(imageFrame.size.width * imageFrame.size.height * 4);
    if (imageBuffer == NULL) {
        DebugLog(@"Warning: Not allocated memory for image for %@", aFilepath);
        CFRelease(newImage);
        return nil;
    }
    memset(imageBuffer, 0, imageFrame.size.width * imageFrame.size.height * 4);
    
    CGColorSpaceRef colourSpace = CGColorSpaceCreateDeviceRGB();
    
    CGContextRef imageContext =
    CGBitmapContextCreate(imageBuffer, imageFrame.size.width, imageFrame.size.height, 8, floor(imageFrame.size.width) * 4, colourSpace, kCGImageAlphaPremultipliedFirst | kCGBitmapByteOrder32Little);
    CGColorSpaceRelease(colourSpace);
    
    if (!imageContext) {
        DebugLog(@"Warning: Not created image context. %@", aFilepath);
        free(imageBuffer);
        CFRelease(newImage);
        return nil;
    }
    
    // draw the image to the context, release it
    if (isnan(anRoundRadius) || anRoundRadius == 0) {
        CGContextDrawImage(imageContext, CGRectMake(0, 0, imageFrame.size.width, imageFrame.size.height), newImage);
    }
    else {
        CGContextSaveGState(imageContext);
        CGPathRef roundAttPath = createPathForRoundedRect(imageFrame, anRoundRadius);
        CGContextAddPath(imageContext, roundAttPath);
        CGContextClip(imageContext);
        CGContextDrawImage(imageContext, CGRectMake(0, 0, imageFrame.size.width, imageFrame.size.height), newImage);
        CGContextRestoreGState(imageContext);
        CGPathRelease(roundAttPath);
    }
    CGImageRelease(newImage);
    
    // draw stroke
    if (useStroke) {
        CGRect borderFrame = CGRectInset(imageFrame, (0 - anStrokeWidth / 2.0f), (0 - anStrokeWidth / 2.0f));
        CGContextSaveGState(imageContext);
        CGPathRef roundPath = createPathForRoundedRect(borderFrame, anRoundRadius);
        CGContextAddPath(imageContext, roundPath);
        CGContextSetLineWidth(imageContext, anStrokeWidth);
        CGContextSetStrokeColorWithColor(imageContext, aStrokeColor.CGColor);
        CGContextDrawPath(imageContext, kCGPathStroke);
        CGContextRestoreGState(imageContext);
        CGPathRelease(roundPath);
    }
    // now get an image ref from the context
    CGImageRef outputImage = CGBitmapContextCreateImage(imageContext);
    
    // post that off to the main thread, where you might do something like
    UIImageOrientation orientation = UIImageOrientationUp;
    
    if (isNeedFixedOrientation) {
        NSURL *aUrl = [NSURL fileURLWithPath:aFilepath];
        CGImageSourceRef source = CGImageSourceCreateWithURL( (__bridge CFURLRef)aUrl, NULL);
        CFDictionaryRef metadata = CGImageSourceCopyPropertiesAtIndex(source,0,NULL);
        NSInteger intOrientation = [(NSNumber *)CFDictionaryGetValue(metadata, kCGImagePropertyOrientation) integerValue];
        CFRelease(metadata);
        orientation = [UIImage UIImageOrientationFromCGOrientation:intOrientation];
        CFRelease(source);
    }
    
    UIImage *uiImage = [UIImage imageWithCGImage:outputImage scale:retinaMultiplier orientation:orientation];
    
    // clean up
    CGImageRelease(outputImage);
    CGContextRelease(imageContext);
    free(imageBuffer);
    
    return uiImage;
}

+ (UIImageOrientation)UIImageOrientationFromCGOrientation:(NSInteger)cgIntegerOrientation {
    switch (cgIntegerOrientation) {
        case 3:
            return UIImageOrientationDown;
            break;
        case 8:
            return UIImageOrientationLeft;
            break;
        case 6:
            return UIImageOrientationRight;
            break;
        case 2:
            return UIImageOrientationUpMirrored;
            break;
        case 4:
            return UIImageOrientationDownMirrored;
            break;
        case 5:
            return UIImageOrientationLeftMirrored;
            break;
        case 7:
            return UIImageOrientationRightMirrored;
            break;
        default:
            return UIImageOrientationUp;
            break;
    }
}

@end
