//
//  UILabel+VerticalAlign.h
//  SpeedyGecko
//
//  Created by Nikolay Chaban on 7/22/14.
//  Copyright (c) 2014 FuzzyZen Labs. All rights reserved.
//

@interface UILabel (VerticalAlign)

// methods

/**
 *  Text vertical alignmet to top of the view
 */
- (void) alignTop;

/**
 *  Text vertical alignment to bottom of the view
 */
- (void) alignBottom;

@end
