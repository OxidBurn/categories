//
//  TextFieldWithColoredPlaceholder.h
//  MetaCert
//
//  Created by Dmitry Svishchov on 27/10/2012.
//  Copyright (c) 2012 MetaCert LLC. All rights reserved.
//


@interface TextFieldWithColoredPlaceholder : UITextField

//! Placeholder color
@property (nonatomic, retain) UIColor* placeholderColor;

- (CGRect) editingRectForBounds: (CGRect) bounds;

/** Clear button position
 */

- (CGRect) clearButtonRectForBounds: (CGRect) bounds;

@end
