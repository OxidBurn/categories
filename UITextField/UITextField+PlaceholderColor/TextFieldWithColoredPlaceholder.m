//
//  TextFieldWithColoredPlaceholder.h
//  MetaCert
//
//  Created by Dmitry Svishchov on 27/10/2012.
//  Copyright (c) 2012 MetaCert LLC. All rights reserved.
//

#import "TextFieldWithColoredPlaceholder.h"


@implementation TextFieldWithColoredPlaceholder


#pragma mark - Draw -


- (CGRect) placeholderRectForBounds: (CGRect) bounds
{
  return [self textRectForBounds: bounds];
}

- (CGRect) editingRectForBounds: (CGRect) bounds
{
  bounds.size.width = bounds.size.width - 10.0f;
  
  return [self textRectForBounds: bounds];
}

- (CGRect) clearButtonRectForBounds: (CGRect) bounds
{
  CGRect rect = [super clearButtonRectForBounds: bounds];
  
  rect.origin.x += 5;
  
  return rect;
}

- (void) drawPlaceholderInRect: (CGRect) rect
{
  if ( [Utils detectOSVersion] >= 7 )
  { // iOS7 and later
    NSDictionary* attributes = @{NSForegroundColorAttributeName: self.placeholderColor, NSFontAttributeName: self.font};
    
    CGRect boundingRect = [self.placeholder boundingRectWithSize: rect.size
                                                         options: 0
                                                      attributes: attributes
                                                         context: nil];
    
    [self.placeholder drawAtPoint: CGPointMake(0, (rect.size.height / 2) - boundingRect.size.height / 2)
                   withAttributes: attributes];
  }
  else
  { // iOS 6
    [self.placeholderColor setFill];
    
    [self.placeholder drawInRect: rect
                        withFont: self.font
                   lineBreakMode: NSLineBreakByTruncatingTail
                       alignment: self.textAlignment];
  }
}

@end
